import { all } from 'redux-saga/effects';
import messagesSagas from '../components/MessageList/sagas';
import loginSagas from '../components/Login/sagas';
import usersSagas from '../components/Users/sagas';

export default function* rootSaga() {
    yield all([
        messagesSagas(),
        loginSagas(),
        usersSagas()
    ])
};
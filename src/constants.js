// export const API_URL = 'https://edikdolynskyi.github.io/react_sources/'
// export const MESSAGES_ENDPOINT = 'messages.json'

export const API_URL = 'http://localhost:3050/api/'
export const MESSAGES_ENDPOINT = 'messages'
export const AUTH_ENDPOINT = 'auth'
export const USERS_ENDPOINT = 'usersChat'
export const TIME_TO_SHOW_ACTIONS_MESSAGES = 5000
import React from 'react'
import { connect } from 'react-redux'
import { Switch, Route, Redirect } from 'react-router-dom'
import { Container, Header, Segment } from 'semantic-ui-react'
import Head from "../../components/Head/Head"
import LoginPage from '../Login/Login'
import ChatPage from "../Chat/Chat"
import UserListPage from '../UserList/UserList'

const Main = ({ authUser }) => {
  return (
    <>
      <Head isAdmin={authUser?.isAdmin} />

      <Switch>
        <Route path="/login" component={LoginPage} />

        {!authUser && <Redirect to="/login" />}

        {authUser?.isAdmin && <Route path='/users/:action?/:userId?' component={UserListPage} />}

        <Redirect from="/users" to="/" />

        <Route path='/' component={ChatPage} />
      </Switch>
      
      <Segment inverted style={{padding: '1.7rem', position:'fixed', width: '100%', bottom: '0px' }}>
        <Container textAlign="center">
          <Header inverted as="h6" content="&copy; Andrii Petrynchyn" />
        </Container>
      </Segment>
    </>
  )
}

const mapStateToProps = ({ auth: { authUser }}) => ({
  authUser
})

export default connect(mapStateToProps)(Main)

import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import MessageList from "../../components/MessageList/MessageList";
import MessageInput from "../../components/MessageInput/MessageInput";

const Chat = () => {
  return (
    <>
      <Switch>
        <Route path='/:action?/:messageId?' component={MessageList} />
      </Switch>

      <MessageInput />
    </>
  )
}

export default Chat

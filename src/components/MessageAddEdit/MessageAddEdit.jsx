import React, { useState, useCallback, useEffect } from 'react'
import { connect } from 'react-redux'
import { Form, Button, Modal, Message } from 'semantic-ui-react'
import { editMessage } from "../../redux/actions"

const MessageAddEdit = ({
  message: { id, text = '' },
  isLoading,
  success_message,
  error_message,
  editMessageAction,
  setLocation
}) => {

  const [textValue, setTextValue] = useState()

  useEffect(() => {
    setTextValue(text)
  }, [text])

  
  const submitHandler = useCallback(() => {
    const message = {
      text: textValue
    }
    editMessageAction(id, message)
  }, [textValue])

  useEffect(() => {
    if (success_message) setTextValue(text)
  }, [success_message])

  const cancelHandler = () => {
    setLocation()
  }

  return (
    <Modal size="mini" closeIcon onClose={() => cancelHandler()} open>
      <Modal.Content style={{ marginBottom: '2rem' }}>
        <Form onSubmit={submitHandler} onReset={() => cancelHandler()} loading={isLoading} success={!!success_message} error={!!error_message} size='tiny'>

          <Form.TextArea autoFocus label='Message' value={textValue} onChange={ev => setTextValue(ev.target.value)}  placeholder='Input Message'/>

          <Message success header='Success' content={success_message} />
          <Message error header='Error' content={error_message} />

          <Button.Group size="mini" compact floated="right">
            <Button type="reset">Cancel</Button>
            <Button.Or />
            <Button type="submit" disabled={textValue === text} secondary>Save</Button>
          </Button.Group>

        </Form>
      </Modal.Content>
    </Modal>
  )
}

const mapStateToProps = ({ chat: { messages, isLoading, success_message, error_message } }, { messageId }) => ({
  message: messages.find(message => message.id === messageId) || {},
  isLoading,
  success_message,
  error_message
})

const mapDispatchToProps = {
  editMessageAction: editMessage
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageAddEdit)

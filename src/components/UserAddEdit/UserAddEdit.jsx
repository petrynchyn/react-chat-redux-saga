import React, { useState, useCallback, useEffect } from 'react'
import { connect } from 'react-redux'
import { Form, Button, Modal, Message, Image, Checkbox, Label } from 'semantic-ui-react'
import { addUser, editUser } from "../../redux/actions"

const UserAddEdit = ({
  user: { id, name = '', login = '', password = '', isAdmin = false, avatar = '' },
  isLoading,
  success_message,
  error_message,
  addUserAction,
  editUserAction,
  setLocation,
  ADD
}) => {

  const [nameValue, setNameValue] = useState()
  const [loginValue, setLoginValue] = useState()
  const [passwordValue, setPasswordValue] = useState()
  const [isAdminValue, setIsAdminValue] = useState()
  const [avatarValue, setAvatarValue] = useState()

  const isNoAnyChanges = () => (
    nameValue === name &&
    loginValue === login &&
    passwordValue === password &&
    isAdminValue == isAdmin &&
    avatarValue === avatar
  )

  useEffect(() => {
    setDefaultUserStates()
  }, [name, login, password, isAdmin, avatar])

  const setDefaultUserStates = () => {
    setNameValue(name)
    setLoginValue(login)
    setPasswordValue(password)
    setIsAdminValue(isAdmin)
    setAvatarValue(avatar)
  }
  
  const submitHandler = useCallback(() => {
    const user = {
      name: nameValue,
      login: loginValue,
      password: passwordValue,
      isAdmin: isAdminValue,
      avatar: avatarValue
    }
    if (id) editUserAction(id, user)
    else  addUserAction(user)
  }, [nameValue, loginValue, passwordValue, isAdminValue, avatarValue])

  useEffect(() => {
    if (success_message) setDefaultUserStates()
  }, [success_message])

  const cancelHandler = () => {
    setLocation()
  }

  return (
    <Modal size="mini" closeIcon onClose={() => cancelHandler()} open>
      <Modal.Content style={{ marginBottom: '2rem' }}>
        <Form onSubmit={submitHandler} onReset={() => cancelHandler()} loading={isLoading} success={!!success_message} error={!!error_message} size='tiny'>

          <Form.Group>
            <Form.Input label='Name' value={nameValue} onChange={ev => setNameValue(ev.target.value)} placeholder='Input Name' />
            <Form.Field>
              <label>Is Admin</label>
              <Form.Checkbox checked={isAdminValue} onChange={(ev, data) => setIsAdminValue(data.checked)} toggle />
            </Form.Field>            
          </Form.Group>
          
          <Form.Group widths='equal'>
            <Form.Input label='Login' value={loginValue} onChange={ev => setLoginValue(ev.target.value)} placeholder='Input Login' />
            <Form.Input label='Password' value={passwordValue} onChange={ev => setPasswordValue(ev.target.value)} type="password" placeholder='Input Password' />
          </Form.Group>

          <Form.Group widths='equal'>
            <Image bordered src={avatarValue} rounded size='tiny' spaced fluid />
            <Form.Input label='Avatar Link' value={avatarValue} onChange={ev => setAvatarValue(ev.target.value)}  placeholder='Input avatar link'/>
          </Form.Group>

          <Message success header='Success' content={success_message} />
          <Message error header='Error' content={error_message} />

          <Button.Group size="mini" compact floated="right">
            <Button type="reset">Cancel</Button>
            <Button.Or />
            <Button type="submit" disabled={isNoAnyChanges()} secondary>{id ? 'Save' : 'Add'}</Button>
          </Button.Group>

        </Form>
      </Modal.Content>
    </Modal>
  )
}

const mapStateToProps = ({ users: { users, isLoading, success_message, error_message } }, { userId }) => ({
  user: users.find(user => user.id === userId) || {},
  isLoading,
  success_message,
  error_message
})

const mapDispatchToProps = {
  editUserAction: editUser,
  addUserAction: addUser
}

export default connect(mapStateToProps, mapDispatchToProps)(UserAddEdit)

import React, { useState, useCallback, useEffect } from 'react'
import { connect } from 'react-redux'
import { useParams } from 'react-router-dom'
import { Container, Divider, Segment, Icon, Comment, Confirm, Loader, Message } from 'semantic-ui-react'
import moment from 'moment'
import { getMessages, delMessage, likeMessage, resetMessageActionsMessages } from "./actions"
import MessageAddEdit from '../MessageAddEdit/MessageAddEdit'
import { TIME_TO_SHOW_ACTIONS_MESSAGES } from "../../constants";

const EDIT = 'edit'
const DELETE = 'delete'

const MessageList = ({ messages, uId, isLoading, success_message, error_message, history, getMessagesAction, delMessageAction, likeMessageAction, resetMessagesActions }) => {

  const { action, messageId } = useParams();
  
  useEffect(() => {
    getMessagesAction()
  }, [])

  useEffect(() => {
    if (success_message || error_message) setTimeout(() => resetMessagesActions(), TIME_TO_SHOW_ACTIONS_MESSAGES)
  }, [success_message, error_message])

  const setLocation = useCallback((param1 = '', param2 = '') => {
    let path = param1 && `/${param1}`
    path += param2 && `/${param2}`
    history.push(path)
  })

  const deleteMessageHandler = useCallback(() => {
    delMessageAction(messageId)
    setLocation()
  }, [messageId, action])


  return (
    <Container text style={{ marginTop: '7rem', marginBottom: '5rem' }}>
      
      <Message success hidden={!success_message} header='Success' content={success_message} onDismiss={resetMessagesActions} size='tiny' style={{ position:'sticky', top: '7rem', zIndex:1 }} />
      <Message error hidden={!error_message} header='Error' content={error_message} onDismiss={resetMessagesActions} size='tiny' style={{ position: 'sticky', top: '7rem', zIndex: 1 }} />

      {messages.map(({ id, user, userId, avatar, text, createdAt, updatedAt, likes = 0 }, index, array) => {
        const curFromNow = moment.utc(createdAt).local().fromNow();
        const prevFromNow = moment.utc(array[index - 1]?.createdAt).local().fromNow();

        return (
          <div key={id}>
            {curFromNow !== prevFromNow
              && <Divider horizontal fitted clearing style={{ 'fontSize': '.7em' }}>{curFromNow}</Divider>
            }
            <Segment
              floated={uId === userId ? 'right' : 'left'}
              style={{ maxWidth: '52%', minWidth: '52%', marginLeft: '1rem', marginRight: '1rem' }}
            >
              <Comment.Group size="small">
                <Comment>
                  {uId !== userId && <Comment.Avatar src={avatar} />}
                  <Comment.Content>
                    <Comment.Author as="a">{user}</Comment.Author>
                    <Comment.Metadata>{updatedAt && <Icon name="edit" corner="top right" />}</Comment.Metadata>
                    <Comment.Metadata>{moment.utc(updatedAt || createdAt).local().format('llll')}</Comment.Metadata>
                    <Comment.Text>{text}</Comment.Text>
                    <Comment.Actions>
                    <Comment.Action onClick={() => uId !== userId && likeMessageAction(id, { likes: likes ? 0 : 1 })}><Icon name="thumbs up" />{likes}</Comment.Action>
                    {uId === userId && <Comment.Action onClick={() => setLocation(EDIT, id)}><Icon name="edit" />Edit</Comment.Action>}
                    {uId === userId && <Comment.Action onClick={() => setLocation(DELETE, id)} icon='trash'><Icon name="trash" />Delete</Comment.Action>}
                  </Comment.Actions>
                  </Comment.Content>
                </Comment>
              </Comment.Group>
            </Segment>

            <Divider hidden fitted clearing />
          </div>
        )
      })}

      <Confirm
        content='Are you sure want to delete this message?'
        cancelButton='Never mind'
        confirmButton="Let's do it"
        size='mini'
        open={action === DELETE}
        onCancel={() => setLocation()}
        onConfirm={deleteMessageHandler}
      />
      
      <Loader active={isLoading}>Loading</Loader>
      
      {action === EDIT && <MessageAddEdit messageId={messageId} setLocation={setLocation} /> }

    </Container>
  )
};

const mapStateToProps = ({ auth: { authUser: { id } }, chat: { messages, isLoading, success_message, error_message} }) => ({
  uId: id,
  messages,
  isLoading,
  success_message,
  error_message
})

const mapDispatchToProps = {
  getMessagesAction: getMessages,
  delMessageAction: delMessage,
  likeMessageAction: likeMessage,
  resetMessagesActions: resetMessageActionsMessages
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageList)

import {
  GET_MESSAGES,
  ADD_MESSAGE,
  EDIT_MESSAGE,
  DEL_MESSAGE,
  LIKE_MESSAGE,
  RESET_MESSAGE_ACTIONS_MESSAGES
} from "./actionTypes"


export const getMessages = () => ({
  type: GET_MESSAGES
})

export const sendMessage = message => ({
  type: ADD_MESSAGE,
  payload: {
    message
  }
})

export const editMessage = (messageId, message) => ({
  type: EDIT_MESSAGE,
  payload: {
    messageId,
    message
  }
})

export const delMessage = (messageId) => ({
  type: DEL_MESSAGE,
  payload: {
    messageId
  }
})

export const likeMessage = (messageId, message) => ({
  type: LIKE_MESSAGE,
  payload: {
    messageId,
    message
  }
})

export const resetMessageActionsMessages = () => ({
  type: RESET_MESSAGE_ACTIONS_MESSAGES
})
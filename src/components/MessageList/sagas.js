import axios from 'axios';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import {
  GET_MESSAGES,
  GET_MESSAGES_STARTED,
  GET_MESSAGES_SUCCESS,
  GET_MESSAGES_FAILURE,
  
  DEL_MESSAGE,
  DEL_MESSAGE_STARTED,
  DEL_MESSAGE_SUCCESS,
  DEL_MESSAGE_FAILURE,

  ADD_MESSAGE,
  ADD_MESSAGE_STARTED,
  ADD_MESSAGE_SUCCESS,
  ADD_MESSAGE_FAILURE,

  EDIT_MESSAGE,
  EDIT_MESSAGE_STARTED,
  EDIT_MESSAGE_SUCCESS,
  EDIT_MESSAGE_FAILURE,

  LIKE_MESSAGE,
  LIKE_MESSAGE_FAILURE

} from "./actionTypes";
import { API_URL, MESSAGES_ENDPOINT } from "../../constants"


export function* fetchMessages(action) {
  try {
    yield put({ type: GET_MESSAGES_STARTED })
    const messages = yield call(axios.get, API_URL + MESSAGES_ENDPOINT)
    yield put({ type: GET_MESSAGES_SUCCESS, payload: { messages: messages.data } })

  } catch (error) {
    yield put({ type: GET_MESSAGES_FAILURE, payload: { error_message: error.response.data.message } })
  }
}

function* watchFetchMessages() {
    yield takeEvery(GET_MESSAGES, fetchMessages)
}


export function* deleteMessage(action) {
  try {
    yield put({ type: DEL_MESSAGE_STARTED })
    yield call(axios.delete, API_URL + MESSAGES_ENDPOINT + '/' + action.payload.messageId)
    yield put({ type: DEL_MESSAGE_SUCCESS, payload: { success_message: 'Message deleted!' } })
    yield put({ type: GET_MESSAGES })

  } catch (error) {
    yield put({ type: DEL_MESSAGE_FAILURE, payload: { error_message: error.response.data.message } })
  }
}

function* watchDeleteMessage() {
  yield takeEvery(DEL_MESSAGE, deleteMessage)
}


export function* addMessage(action) {
  try {
    yield put({ type: ADD_MESSAGE_STARTED })
    yield call(axios.post, API_URL + MESSAGES_ENDPOINT, action.payload.message)
    yield put({ type: ADD_MESSAGE_SUCCESS, payload: { success_message: 'Message added!' } })
    yield put({ type: GET_MESSAGES })
  
  } catch (error) {
    yield put({ type: ADD_MESSAGE_FAILURE, payload: { error_message: error.response.data.message } })
  }
}

function* watchAddMessage() {
  yield takeEvery(ADD_MESSAGE, addMessage)
}


export function* editMessage(action) {
  try {
    yield put({ type: EDIT_MESSAGE_STARTED })
    yield call(axios.put, API_URL + MESSAGES_ENDPOINT + `/${action.payload.messageId}`, action.payload.message)
    yield put({ type: EDIT_MESSAGE_SUCCESS, payload: { success_message: 'Message edited!' } })
    yield put({ type: GET_MESSAGES })
  
  } catch (error) {
    yield put({ type: EDIT_MESSAGE_FAILURE, payload: { error_message: error.response.data.message } })
  }
}

function* watchEditMessage() {
  yield takeEvery(EDIT_MESSAGE, editMessage)
}


export function* likeMessage(action) {
  try {
    yield call(axios.put, API_URL + MESSAGES_ENDPOINT + `/${action.payload.messageId}`, action.payload.message)
    yield put({ type: GET_MESSAGES })
  
  } catch (error) {
    yield put({ type: LIKE_MESSAGE_FAILURE, payload: { error_message: error.response.data.message } })
  }
}

function* watchLikeMessage() {
  yield takeEvery(LIKE_MESSAGE, likeMessage)
}


export default function* messagesPageSagas() {
    yield all([
      watchFetchMessages(),
      watchAddMessage(),
      watchEditMessage(),
      watchDeleteMessage(),
      watchLikeMessage()
    ])
};

import {
  GET_MESSAGES_STARTED,
  GET_MESSAGES_SUCCESS,
  GET_MESSAGES_FAILURE,
  DEL_MESSAGE_STARTED,
  DEL_MESSAGE_SUCCESS,
  DEL_MESSAGE_FAILURE,
  EDIT_MESSAGE_STARTED,
  EDIT_MESSAGE_SUCCESS,
  EDIT_MESSAGE_FAILURE,
  ADD_MESSAGE_FAILURE,
  ADD_MESSAGE_SUCCESS,
  ADD_MESSAGE_STARTED,
  LIKE_MESSAGE_FAILURE,
  RESET_MESSAGE_ACTIONS_MESSAGES

} from "./actionTypes";

const initialState = {
  messages: [],
  isLoading: false,
  success_message: '',
  error_message: ''
};

export default function (state = initialState, action) {
  switch (action.type) {

    case GET_MESSAGES_SUCCESS: {
      return {
        ...state,
        messages: action.payload.messages,
        isLoading: false,
        error_message: ''
      }
    }

    case ADD_MESSAGE_STARTED:
    case EDIT_MESSAGE_STARTED:
    case DEL_MESSAGE_STARTED:
    case GET_MESSAGES_STARTED: {
      return {
        ...state,
        isLoading: true,
        error_message: ''
      }
    }
      
    case ADD_MESSAGE_SUCCESS:
    case EDIT_MESSAGE_SUCCESS:
    case DEL_MESSAGE_SUCCESS: {
      return {
        ...state,
        success_message: action.payload.success_message,
        error_message: ''
      }
    }
    
    case LIKE_MESSAGE_FAILURE:
    case ADD_MESSAGE_FAILURE:
    case EDIT_MESSAGE_FAILURE:
    case DEL_MESSAGE_FAILURE:
    case GET_MESSAGES_FAILURE: {
      return {
        ...state,
        isLoading: false,
        success_message: '',
        error_message: action.payload.error_message
      }
    }
      
    case RESET_MESSAGE_ACTIONS_MESSAGES: {
      return {
        ...state,
        success_message: '',
        error_message: ''
      }
    }
      
    default:
      return state
  }
}

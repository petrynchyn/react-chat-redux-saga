import React, { useState, useCallback } from 'react'
import { connect } from 'react-redux'
import { sendMessage } from "../../redux/actions";
import { Container, Form, Button } from 'semantic-ui-react'

const MessageInput = ({ authUser, sendMessageAction }) => {

  const [msgText, setMsgText] = useState('')

  const sendMsgHandler = useCallback(
    () => {
      const newMsg = {
        userId: authUser.id,
        avatar: authUser.avatar,
        user: authUser.name,
        text: msgText,
        likes: 0
      }
      sendMessageAction(newMsg)
      setMsgText('')
    }, [authUser, msgText]
  )

  return (
    <Container text  style={{ position:'sticky', bottom: '4rem', backgroundColor: 'white', paddingBottom: '1rem'}}>
      <Form onSubmit={sendMsgHandler} widths="equal">
        <Form.Group>
          <Form.TextArea autoFocus value={msgText} onChange={ev => setMsgText(ev.target.value)} placeholder='Message' />
          <Button type='submit' disabled={!msgText.length} secondary>Send</Button>
        </Form.Group>
      </Form>
    </Container>
  )
}

const mapStateToProps = ({ auth: { authUser } }) => ({
  authUser
})

const mapDispatchToProps = {
  sendMessageAction: sendMessage 
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput)

import { AUTH_USER_STARTED, AUTH_USER_SUCCESS, AUTH_USER_FAILURE } from "./actionTypes";
import { getObjectFromLocalStorage } from "../../helpers/localStorageHelper";

const initialState = {
    authUser: getObjectFromLocalStorage('user'),
    isLoading: false,
    error_message: ''
};

export default function (state = initialState, action) {
switch (action.type) {
    case AUTH_USER_SUCCESS: {
        const { authUser } = action.payload;
        return {
            ...state,
            authUser
        };
    }
    
    case AUTH_USER_STARTED: {
        return {
        ...state,
        isLoading: true
        }
    }

    case AUTH_USER_FAILURE: {
        return {
        ...state,
        isLoading: false,
        error_message: action.payload.error_message
        }
    }

    default:
        return state;
}
}

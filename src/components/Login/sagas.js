import axios from 'axios';
import { API_URL, AUTH_ENDPOINT } from "../../constants"
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { AUTH_USER, AUTH_USER_STARTED, AUTH_USER_SUCCESS, AUTH_USER_FAILURE } from "./actionTypes";

export function* authUser(action) {
    try {
        yield put({ type: AUTH_USER_STARTED })
        const messages = yield call(axios.post, API_URL + AUTH_ENDPOINT, action.payload)
        yield put({ type: AUTH_USER_SUCCESS, payload: { authUser: messages.data } })
    
      } catch (error) {
        yield put({ type: AUTH_USER_FAILURE, payload: { error_message: error.response.data.message } })
      }
}

function* watchAuthUser() {
    yield takeEvery(AUTH_USER, authUser)
}

export default function* loginPageSagas() {
    yield all([
        watchAuthUser()
    ])
};

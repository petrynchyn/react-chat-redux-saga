import React, { useState, useCallback, useEffect } from 'react'
import { connect } from 'react-redux'
import { loginUser } from "../../redux/actions";
import { Modal, Form, Button, Message } from 'semantic-ui-react'
import { setLocalStorageItem } from "../../helpers/localStorageHelper";


const Login = ({ authUser, isLoading, error_message, loginUserAction, history}) => {

  const [login, setLogin] = useState('')
  const [password, setPassword] = useState('')

  const loginUserHandler = useCallback(() => {
    loginUserAction({ login, password })
  }, [login, password])

  useEffect(() => {
    if (authUser) {
      setLocalStorageItem('user', authUser)
      history.push(authUser.isAdmin ? '/users' : '/');
    }
  }, [authUser])

  return (
    <div style={{ minHeight: '100vh' }}>
      <Modal size="mini" open  dimmer="blurring">
        <Modal.Content>
          <Form
            error={!!error_message}
            onSubmit={loginUserHandler}
          >
            <Form.Input
              autoFocus
              label='Login'
              placeholder='Input login'
              value={login}
              onChange={ev => setLogin(ev.target.value)}
            />
            <Form.Input
              label='Password'
              placeholder='Input password'
              type="password"
              value={password}
              onChange={ev => setPassword(ev.target.value)}
            />
            <Message
              error
              header='Login Failed'
              content={error_message}
            />
            <Button
              type='submit'
              content='Login'
              loading={!!isLoading}
              disabled={!login.length || !password.length}
              secondary
            />
          </Form>
        </Modal.Content>
      </Modal>
    </div>
  )
}

const mapStateToProps = ({ auth: { authUser, isLoading, error_message }}) => ({
  authUser,
  isLoading,
  error_message
})

const mapDispatchToProps = {
  loginUserAction: loginUser
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)

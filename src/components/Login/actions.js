import { AUTH_USER } from "./actionTypes";

export const loginUser = ({ login, password }) => ({
    type: AUTH_USER,
    payload: {
        login,
        password
    }
});
import React, { useEffect, useCallback } from 'react'
import { connect } from 'react-redux'
import { useParams } from 'react-router-dom'
import { Container, Table, Button, Confirm, Icon, Image, Loader, Message } from 'semantic-ui-react'
import { getUsers, delUser, resetUserActionsMessages } from "../../redux/actions"
import UserAddEdit from '../UserAddEdit/UserAddEdit'
import { TIME_TO_SHOW_ACTIONS_MESSAGES } from "../../constants";

const ADD = 'add'
const EDIT = 'edit'
const DELETE = 'delete'

const UserList = ({ users, isLoading, success_message, error_message, history, getUsersAction, delUserAction, resetUserMessagesAction }) => {

  const { action, userId } = useParams();

  useEffect(() => {
    getUsersAction()
  }, [])

  useEffect(() => {
    if (success_message || error_message) setTimeout(() => resetUserMessagesAction(), TIME_TO_SHOW_ACTIONS_MESSAGES)
  }, [success_message, error_message])

  const setLocation = useCallback((param1 = '', param2 = '') => {
    let path = '/users'
    path += param1 && `/${param1}`
    path += param2 && `/${param2}`
    history.push(path)
  })

  const deleteUserHandler = useCallback(() => {
    delUserAction(userId)
    setLocation()
  }, [userId, action])

  return (
    <Container text style={{ marginTop: '10rem', marginBottom: '2em', fontSize: '1rem' }}>
      <Message success hidden={!success_message} header='Success' content={success_message} onDismiss={resetUserMessagesAction} size='tiny' />
      <Message error hidden={!error_message} header='Error' content={error_message} onDismiss={resetUserMessagesAction} size='tiny' />
      <Table  basic='very' columns={5}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell />
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Login</Table.HeaderCell>
            <Table.HeaderCell>Admin</Table.HeaderCell>
            <Table.HeaderCell textAlign='center'>
              <Button onClick={() => setLocation(ADD)} icon secondary size='mini' labelPosition='left'>
                <Icon name='user' />
                Add User
              </Button>
            </Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>

          {users.map(({ id, name, login, avatar, isAdmin }) => (
            <Table.Row key={id}>
              <Table.Cell textAlign='center'><Image src={avatar} rounded size='tiny' verticalAlign='middle' /></Table.Cell>
              <Table.Cell>{name}</Table.Cell>
              <Table.Cell>{login}</Table.Cell>
              <Table.Cell><Icon color='green' name={isAdmin ? 'user' : 'user outline'} size='large' /></Table.Cell>
              <Table.Cell textAlign='center'>
                <Icon name='edit' size='large' link onClick={() => setLocation(EDIT, id)}/>
                <Icon name='delete' size='large' link onClick={() => setLocation(DELETE, id)}/>
              </Table.Cell>
            </Table.Row>
          ))}

        </Table.Body>

      </Table>

      <Confirm
        content='Are you sure want to delete this user?'
        cancelButton='Never mind'
        confirmButton="Let's do it"
        size='mini'
        open={action === DELETE}
        onCancel={() => setLocation()}
        onConfirm={deleteUserHandler}
      />

      <Loader active={isLoading}>Loading</Loader>

      { (action === ADD || action === EDIT) && <UserAddEdit userId={userId} ADD={ADD} setLocation={setLocation} /> }

    </Container>
  )
}

const mapStateToProps = ({ users: { users, isLoading, success_message, error_message } }) => ({
  users,
  isLoading,
  success_message,
  error_message
})

const mapDispatchToProps = {
  getUsersAction: getUsers,
  delUserAction: delUser,
  resetUserMessagesAction: resetUserActionsMessages
}

export default connect(mapStateToProps, mapDispatchToProps)(UserList)

import {
  GET_USERS,
  ADD_USER,
  EDIT_USER,
  DEL_USER,
  RESET_USER_ACTIONS_MESSAGES

} from "./actionTypes"


export const getUsers = () => ({
  type: GET_USERS
})

export const addUser = user => ({
  type: ADD_USER,
  payload: {
    user
  }
})

export const editUser = (userId, user) => ({
  type: EDIT_USER,
  payload: {
    userId,
    user
  }
})

export const delUser = id => ({
  type: DEL_USER,
  payload: {
    id
  }
})

export const resetUserActionsMessages = () => ({
  type: RESET_USER_ACTIONS_MESSAGES
})
import axios from 'axios';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import {
  GET_USERS,
  GET_USERS_STARTED,
  GET_USERS_SUCCESS,
  GET_USERS_FAILURE,

  DEL_USER,
  DEL_USER_STARTED,
  DEL_USER_SUCCESS,
  DEL_USER_FAILURE,

  ADD_USER,
  ADD_USER_STARTED,
  ADD_USER_SUCCESS,
  ADD_USER_FAILURE,

  EDIT_USER,
  EDIT_USER_STARTED,
  EDIT_USER_SUCCESS,
  EDIT_USER_FAILURE

} from "./actionTypes";
import { API_URL, USERS_ENDPOINT } from "../../constants"


export function* fetchUsers() {
  try {
    yield put({ type: GET_USERS_STARTED })
    const users = yield call(axios.get, API_URL + USERS_ENDPOINT)
    yield put({ type: GET_USERS_SUCCESS, payload: { users: users.data } })

  } catch (error) {
    yield put({ type: GET_USERS_FAILURE, payload: { error_message: error.response.data.message } })
  }
}

function* watchFetchUsers() {
  yield takeEvery(GET_USERS, fetchUsers)
}


export function* deleteUser(action) {
  try {
    yield put({ type: DEL_USER_STARTED })
    yield call(axios.delete, API_URL + USERS_ENDPOINT + '/' + action.payload.id)
    yield put({ type: DEL_USER_SUCCESS, payload: { success_message: 'User deleted!' } })
    yield put({ type: GET_USERS })

  } catch (error) {
    yield put({ type: DEL_USER_FAILURE, payload: { error_message: error.response.data.message } })
  }
}

function* watchDeleteUser() {
  yield takeEvery(DEL_USER, deleteUser)
}


export function* addUser(action) {
  try {
    yield put({ type: ADD_USER_STARTED })
    yield call(axios.post, API_URL + USERS_ENDPOINT, action.payload.user)
    yield put({ type: ADD_USER_SUCCESS, payload: { success_message: 'User added!' } })
    yield put({ type: GET_USERS })
  
  } catch (error) {
    yield put({ type: ADD_USER_FAILURE, payload: { error_message: error.response.data.message } })
  }
}

function* watchAddUser() {
  yield takeEvery(ADD_USER, addUser)
}


export function* editUser(action) {
  try {
    yield put({ type: EDIT_USER_STARTED })
    yield call(axios.put, API_URL + USERS_ENDPOINT + `/${action.payload.userId}`, action.payload.user)
    yield put({ type: EDIT_USER_SUCCESS, payload: { success_message: 'User edited!' } })
    yield put({ type: GET_USERS })
  
  } catch (error) {
    yield put({ type: EDIT_USER_FAILURE, payload: { error_message: error.response.data.message } })
  }
}

function* watchEditUser() {
  yield takeEvery(EDIT_USER, editUser)
}


export default function* usersPageSagas() {
  yield all([
    watchFetchUsers(),
    watchAddUser(),
    watchEditUser(),
    watchDeleteUser()
  ])
};

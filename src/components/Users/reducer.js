import {
  GET_USERS_STARTED,
  GET_USERS_SUCCESS,
  GET_USERS_FAILURE,
  DEL_USER_STARTED,
  DEL_USER_SUCCESS,
  DEL_USER_FAILURE,
  EDIT_USER_STARTED,
  EDIT_USER_SUCCESS,
  EDIT_USER_FAILURE,
  ADD_USER_FAILURE,
  ADD_USER_SUCCESS,
  ADD_USER_STARTED,
  RESET_USER_ACTIONS_MESSAGES

} from "./actionTypes";

const initialState = {
  users: [],
  isLoading: false,
  success_message: '',
  error_message: ''
};

export default function (state = initialState, action) {
  switch (action.type) {
  
    case GET_USERS_SUCCESS: {
      return {
        ...state,
        users: action.payload.users,
        isLoading: false,
        error_message: ''
      }
    }

    case ADD_USER_STARTED:
    case EDIT_USER_STARTED:
    case DEL_USER_STARTED:
    case GET_USERS_STARTED: {
      return {
        ...state,
        isLoading: true,
        error_message: ''
      }
    }
      
    case ADD_USER_SUCCESS:
    case EDIT_USER_SUCCESS:
    case DEL_USER_SUCCESS: {
      return {
        ...state,
        success_message: action.payload.success_message,
        error_message: ''
      }
    }
  
    case ADD_USER_FAILURE:
    case EDIT_USER_FAILURE:
    case DEL_USER_FAILURE:
    case GET_USERS_FAILURE: {
      return {
        ...state,
        isLoading: false,
        success_message: '',
        error_message: action.payload.error_message
      }
    }
      
    case RESET_USER_ACTIONS_MESSAGES: {
      return {
        ...state,
        success_message: '',
        error_message: ''
      }
    }
      
    default:
      return state
  }
}

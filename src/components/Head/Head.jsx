import React from 'react'
import { connect } from 'react-redux'
import { Container, Menu, Step } from 'semantic-ui-react'
import moment from 'moment'
import { useLocation } from 'react-router'

const Head = ({ messagesCount, participantsCount = '?', lastMessageDate, isAdmin=false }) => (
  <Menu fixed='top' inverted>
    <Container text>
      <Step.Group widths={4} size='mini'>

        <Step title="Chat" active link href={ useLocation().pathname === '/' && isAdmin ? '/users' : '/'  } icon="comment alternate outline" />

        <Step description="PARTICIPANTS" icon="users" title={participantsCount} />
        {useLocation().pathname === '/'
          ?<Step description="MESSAGES" icon="address card outline" title={messagesCount} />
          :<Step />
        }
        {useLocation().pathname === '/'
          ? <Step description="LAST MESSAGE" icon="clock outline" title={moment.utc(lastMessageDate).local().fromNow()} />
          : <Step />
        }

      </Step.Group>
    </Container> 
  </Menu>
)


const mapStateToProps = ({ chat: { messages }, users: {users} }) => ({
  messagesCount: messages.length,
  lastMessageDate: messages[messages.length - 1]?.createdAt,
  participantsCount: () => {
    const participants = new Set()
    messages.forEach(element => participants.add(element.userId))
    return participants.size || users.length
  }
})

export default connect(mapStateToProps)(Head)

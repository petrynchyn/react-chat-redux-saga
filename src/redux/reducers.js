import { combineReducers } from "redux";
import chat from "../components/MessageList/reducer";
import auth from "../components/Login/reducer";
import users from "../components/Users/reducer";

export default combineReducers({ chat, auth, users });
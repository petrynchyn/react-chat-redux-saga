const express = require('express')
const cors = require('cors');
const { responseErrorMiddleware } = require('./middlewares/response.middleware');
require('./config/db');

const app = express()

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const routes = require('./routes/index');
routes(app);

app.use('/', express.static('./client/build'));
app.use((req, res) => {
    res.status(404).json({
        error: true,
        message: 'Page not found'
    });
});

app.use(responseErrorMiddleware);

const port = process.env.PORT || 3050;
app.listen(port, () => { console.log(`Server started on port ${port}`);});

exports.app = app;
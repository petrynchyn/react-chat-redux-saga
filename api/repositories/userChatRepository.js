const { BaseRepository } = require('./baseRepository');

class UserChatRepository extends BaseRepository {
    constructor() {
        super('usersChat');
    }
}

exports.UserChatRepository = new UserChatRepository();
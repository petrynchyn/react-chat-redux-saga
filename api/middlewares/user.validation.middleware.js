const { user } = require('../models/user');

function isValidEmail(data) {
    return /^[a-z0-9](\.?[a-z0-9]){5,29}@g(oogle)?mail\.com$/.test(data)
}

function isValidPhoneNumber(data) {
    return /^\+380\d{9}$/.test(data)
}

function isValidFirstName(data) {
    return (data.length >= 0) ? true : false
}

function isValidLastName(data) {
    return (data.length >= 0) ? true : false
}

function isValidPassword(data) {
    return (data.length > 2) ? true : false
}

function isHasUnnecessaryFields(data, model) {
    
    const dataKeys = Object.getOwnPropertyNames(data)
    const modelKeys = Object.getOwnPropertyNames(model)

    return  dataKeys.some((a) => !modelKeys.includes(a) )
}

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation

    res.err = ''

    delete user.id

    if (isHasUnnecessaryFields(req.body, user)) {
        res.err += 'Сontains unnecessary fields. '
    }
    
    if ('id' in req.body ) {
        res.err += 'ID cannot be in body. '
    }

    if ('firstName' in req.body && isValidFirstName(req.body.firstName)) {
        user.firstName = req.body.firstName
    } else {
        res.err += 'First name should not be empty. '
    }

    if ('lastName' in req.body && isValidLastName(req.body.lastName)) {
        user.lastName = req.body.lastName
    } else {
        res.err += 'Last name should not be empty. '
    }
    
    if ('email' in req.body && isValidEmail(req.body.email)) {
        user.email = req.body.email
    } else {
        res.err += 'Email is not valid. '
    }
    
    if ('phoneNumber' in req.body && isValidPhoneNumber(req.body.phoneNumber)) {
        user.phoneNumber = req.body.phoneNumber
    } else {
        res.err += 'Phone number is not valid. '
    }    

    if ('password' in req.body && isValidPassword(req.body.password)) {
        user.password = req.body.password
    } else {
        res.err += 'Password must be 3 or more characters. '
    }

    if (res.err) {
        throw Error(res.err)
    }

    req.body = user
    
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    res.err = ''
    
    delete user.id
    
    if (isHasUnnecessaryFields(req.body, user)) {
        res.err += 'Сontains unnecessary fields. '
    }

    if ('id' in req.body) {
        res.err += 'ID cannot be in body. '
    }
    
    if ('firstName' in req.body) {
        if (isValidFirstName(req.body.firstName)) {
            user.firstName = req.body.firstName
        } else {
            res.err += 'First name should not be empty. '
        }
    } else {
        delete user.firstName
    }
        
    if ('lastName' in req.body) {
        if (isValidLastName(req.body.lastName)) { 
            user.lastName = req.body.lastName
        } else {
            res.err += 'Last name should not be empty. '
        }
    } else {
        delete user.lastName
    }

    if ('email' in req.body) {
        if (isValidEmail(req.body.email)) {
            user.email = req.body.email
        } else {
            res.err += 'Email is not valid. '
        }
    } else {
        delete user.email
    }
        
    if ('phoneNumber' in req.body) {
        if (isValidPhoneNumber(req.body.phoneNumber)) {
            user.phoneNumber = req.body.phoneNumber
        } else {
            
            res.err += 'Phone number is not valid. '
        }
    } else {
        delete user.phoneNumber
    }
        
    if ('password' in req.body) {
        if (isValidPassword(req.body.password)) { 
            user.password = req.body.password
        } else {
            res.err += 'Password must be 3 or more characters. '
        }
    } else {
        delete user.password
    }
    
    if (res.err) {
        throw Error(res.err)
    }

    req.body = user

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
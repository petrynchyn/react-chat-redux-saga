const { fighter } = require('../models/fighter');

function isValidName(data) {
    return (data.length >= 0) ? true : false
}

function isValidHealth(data) {
    return (typeof data === 'number' && data >= 0) ? true : false
}

function isValidPower(data) {
    return (typeof data === 'number' && data >= 0 && data < 100) ? true : false
}

function isValidDefense(data) {
    return (typeof data === 'number' && data >= 1 && data <= 10) ? true : false
}

function isHasUnnecessaryFields(data, model) {

    const dataKeys = Object.getOwnPropertyNames(data)
    const modelKeys = Object.getOwnPropertyNames(model)

    return dataKeys.some((a) => !modelKeys.includes(a))
}

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation

    res.err = ''

    delete fighter.id
    
    if (isHasUnnecessaryFields(req.body, fighter)) {
        res.err += 'Сontains unnecessary fields. '
    }

    if ('id' in req.body) {
        res.err += 'ID cannot be in body. '
    }

    if ('name' in req.body && isValidName(req.body.name)) {
        fighter.name = req.body.name
    } else {
        res.err += 'Name should not be empty. '
    }


    if ('health' in req.body) {

        if (isValidHealth(req.body.health)) {
            fighter.health = req.body.health
        } else {
            res.err += 'Health must be > 0. '
        }
    }
 

    if ('power' in req.body && isValidPower(req.body.power)) {
        fighter.power = req.body.power
    } else {
        res.err += 'Power must be > 0 & < 100. '
    }   


    if ('defense' in req.body && isValidDefense(req.body.defense)) {
        fighter.defense = req.body.defense
    } else {
        res.err += 'Defense must be from 1 to 10. '
    }


    if (res.err) {
        throw Error(res.err)
    }

    req.body = fighter
    
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update

    res.err = ''

    delete fighter.id

    if (isHasUnnecessaryFields(req.body, fighter)) {
        res.err += 'Сontains unnecessary fields. '
    }

    if ('id' in req.body) {
        res.err += 'ID cannot be in body. '
    }

    if ('name' in req.body) {

        if (isValidName(req.body.name)) {
            fighter.name = req.body.name
        } else {
            res.err += 'Name should not be empty. '
        }
    } else {
        delete fighter.name
    }


    if ('health' in req.body) {

        if (isValidHealth(req.body.health)) {
            fighter.health = req.body.health
        } else {
            res.err += 'Health must be > 0. '
        }
    } else {
        delete fighter.health
    }


    if ('power' in req.body) {

        if (isValidPower(req.body.power)) {
            fighter.power = req.body.power
        } else {
            res.err += 'Power must be > 0 & < 100. '
        }
    } else {
        delete fighter.power
    }   


    if ('defense' in req.body) {

        if (isValidDefense(req.body.defense)) {
            fighter.defense = req.body.defense
        }  else {
            res.err += 'Defense must be from 1 to 10. '
        }
    } else {
        delete fighter.defense
    }

    
    if (res.err) {
        throw Error(res.err)
    }

    req.body = fighter

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
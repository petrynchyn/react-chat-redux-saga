const responseMiddleware = (req, res) => {
   // TODO: Implement middleware that returns result of the query

    if (!res.data) throw Error('Bad request. ')
    res.json(res.data);
}

const responseErrorMiddleware = (err, req, res, next) => {
   // TODO: Implement middleware that returns error of the query
  
    let errCode = 400
    if (res.errCode) errCode = res.errCode

    res.status(errCode).json({
        error: true,
        message: err.message
    });

    next(err);
}

exports.responseMiddleware = responseMiddleware;
exports.responseErrorMiddleware = responseErrorMiddleware;
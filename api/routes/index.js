const userRoutes = require('./userRoutes');
const userChatRoutes = require('./userChatRoutes');
const authRoutes = require('./authRoutes');
const fighterRoutes = require('./fighterRoutes');
const fightRoutes = require('./fightRoutes');
const messageRoutes = require('./messageRoutes');
const { responseMiddleware } = require('../middlewares/response.middleware');

module.exports = (app) => {
  app.use('/api/messages', messageRoutes, responseMiddleware);
  app.use('/api/users', userRoutes, responseMiddleware);
  app.use('/api/usersChat', userChatRoutes, responseMiddleware);
  app.use('/api/fighters', fighterRoutes, responseMiddleware);
  app.use('/api/fights', fightRoutes, responseMiddleware);
  app.use('/api/auth', authRoutes, responseMiddleware);
};
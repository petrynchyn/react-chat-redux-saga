const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', (req, res, next) => {

    res.data = UserService.getAll()
    
    next();

});


router.get('/:id', (req, res, next) => {
    
    res.errCode = 404

    res.data = UserService.search({id: req.params.id})
    
    next();

});


router.post('/', createUserValid, (req, res, next) => {

    res.data = UserService.create(req.body)
  
    next();

});


router.put('/:id', updateUserValid, (req, res, next) => {

    res.errCode = 404

    res.data = UserService.update(req.params.id, req.body)

    next();

});


router.delete('/:id', (req, res, next) => {
    
    res.errCode = 404
    
    res.data = UserService.delete(req.params.id)
    
    next();

});

module.exports = router;
const { Router } = require('express');
const UserChatService = require('../services/userChatService');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', (req, res, next) => {

    res.data = UserChatService.getAll()
    
    next();

});


router.get('/:id', (req, res, next) => {
    
    res.errCode = 404

    res.data = UserChatService.search({id: req.params.id})
    
    next();

});


router.post('/', (req, res, next) => {

    res.data = UserChatService.create(req.body)
  
    next();

});


router.put('/:id', (req, res, next) => {

    res.errCode = 404

    res.data = UserChatService.update(req.params.id, req.body)

    next();

});


router.delete('/:id', (req, res, next) => {
    
    res.errCode = 404
    
    res.data = UserChatService.delete(req.params.id)
    
    next();

});

module.exports = router;
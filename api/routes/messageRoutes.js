const { Router } = require('express');
const MessageService = require('../services/messageService');

const router = Router();

// TODO: Implement route controllers for message

router.get('/', (req, res, next) => {

    res.data = MessageService.getAll()
    
    next();

});


router.get('/:id', (req, res, next) => {

    res.errCode = 404

    res.data = MessageService.search({id: req.params.id})
    
    next();

});


router.post('/', (req, res, next) => {

    res.data = MessageService.create(req.body)
  
    next();

});


router.put('/:id', (req, res, next) => {

    res.errCode = 404
    
    res.data = MessageService.update(req.params.id, req.body)

    next();

});


router.delete('/:id', (req, res, next) => {

    res.errCode = 404

    res.data = MessageService.delete(req.params.id)
    
    next();

});

module.exports = router;
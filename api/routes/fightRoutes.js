const { Router } = require('express');
const FightService = require('../services/fightService');


const router = Router();

// OPTIONAL TODO: Implement route controller for fights

router.get('/:idF1/:idF2', (req, res, next) => {

    res.errCode = 404

    res.data = FightService.getFights({fighter1: req.params.idF1, fighter2: req.params.idF2})
    
    next();

});

router.post('/', (req, res, next) => {

    res.data = FightService.create(req.body)
  
    next();

});

module.exports = router;
const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', (req, res, next) => {

    res.data = FighterService.getAll()
    
    next();

});


router.get('/:id', (req, res, next) => {

    res.errCode = 404

    res.data = FighterService.search({id: req.params.id})
    
    next();

});


router.post('/', createFighterValid, (req, res, next) => {

    res.data = FighterService.create(req.body)
  
    next();

});


router.put('/:id', updateFighterValid, (req, res, next) => {

    res.errCode = 404
    
    res.data = FighterService.update(req.params.id, req.body)

    next();

});


router.delete('/:id', (req, res, next) => {

    res.errCode = 404

    res.data = FighterService.delete(req.params.id)
    
    next();

});

module.exports = router;
const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {

        const item = UserRepository.getOne(search);
        if(!item) {
            throw Error('User not found. ')
        }
        return item;
    }

    getAll() {
        return UserRepository.getAll();
    }

    create(data) {

        let item = UserRepository.getOne({email: data.email});
        if(item) {
            throw Error(`User with email ${data.email} already registered. `)
        }

        item = UserRepository.create(data);
        if(!item) {
            throw Error('Add user failed. ')
        }
        return item;
    }

    update(id, data) {

        let item = UserRepository.update(id, data);
        if(!item) {
            throw Error('Update user failed, user not found. ')
        }
        return item;
    }
    
    delete(id) {

        let item =  UserRepository.delete(id);
        if (!item.length) {
            throw Error('Delete user failed, user not found. ')
        }
        return item;
    }

}

module.exports = new UserService();
const { UserChatRepository } = require('../repositories/userChatRepository');

class UserChatService {

    // TODO: Implement methods to work with user

    search(search) {

        const item = UserChatRepository.getOne(search);
        if(!item) {
            throw Error('User not found. ');
        }
        return item;
    }

    getAll() {
        return UserChatRepository.getAll();
    }

    create(data) {

        let item = UserChatRepository.getOne({login: data.login});
        if(item) {
            throw Error(`User with login ${data.login} already registered. `);
        }

        item = UserChatRepository.create(data);
        if(!item) {
            throw Error('Add user failed. ');
        }
        return item;
    }

    update(id, data) {

        let item = UserChatRepository.update(id, data);
        if(!item) {
            throw Error('Update user failed, user not found. ');
        }
        return item;
    }
    
    delete(id) {

        let item =  UserChatRepository.delete(id);
        if (!item.length) {
            throw Error('Delete user failed, user not found. ');
        }
        return item;
    }

}

module.exports = new UserChatService();